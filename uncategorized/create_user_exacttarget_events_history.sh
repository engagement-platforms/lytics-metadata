# create/update a query 
# - ALIAS must be unique (uses as id to upsert)
curl -s -H "Authorization: $LIOKEY" -H "Content-Type: text/plain" \
  -XPOST "$LIOAPI/api/query" -d '
/*
ExactTarget User-Event data (historical load from Redshift) such as clicks, open, sends.

*/
SELECT
    set(subscriber_key)        AS et_subscriber_keys
    , valuect(event_type)      AS et_events
    , set(url)                 AS et_click_urls                                        SHORTDESC "ET Urls Clicked on"
    , valuect(hash(urlmain(url)))  AS hashedurls        IF eq(event_type, "open") OR eq(event_type, "click")  SHORTDESC "Hashed Urls Visited"  LONGDESC "Count of how many times a hashed url was visited"
    , bounce_category          AS et_bounce_category                                   SHORTDESC "ET Bounce Category"      LONGDESC "Defines category for bounce associated with a bounced email."
    , bounce_type              AS et_bounce_type                                       SHORTDESC "ET Bounce Type"          LONGDESC "Defines type of bounce associated with a bounced email."
    , smtp_reason              AS et_smtp_bounce_reason                                SHORTDESC "ET SMTP Bounce Reason"   LONGDESC "Contains SMTP reason associated with a bounced email."
    , survey_question          AS et_survey_question                                   SHORTDESC "ET Survey Question"      LONGDESC "Specifies question associated with a survey event."
    , survey_answer            AS et_survey_answer                                     SHORTDESC "ET Survey Answer"        LONGDESC "The answer provided by a subscriber to the survey question."
    , list_name                AS et_list_name                                         SHORTDESC "ET List Name"            LONGDESC "The list name in an associated event."
    , opt_in_sub_key           AS et_opt_in_sub_key                                    SHORTDESC "ET Forward Opt-In"       LONGDESC "Specifies the subscriber key of a subscriber opted in via forwarded email."
    , send_id                  AS et_send_id                                           SHORTDESC "ET Send ID"              LONGDESC "Specifies the ID of a send associated with an event."

    , epochms()                AS et_lastopen_ts        IF eq(event_type, "open")      SHORTDESC "ET Last Open"            LONGDESC "Last time user opened email"        KIND DATE
    , epochms()                AS last_active_ts        IF eq(event_type, "open")      SHORTDESC "Last Active"             KIND DATE
    , min(epochms())           AS et_firstopen_ts       IF eq(event_type, "open")      SHORTDESC "ET First Open"           LONGDESC "First time user opened email"       KIND DATE
    , count(event_type)        AS et_openct             IF eq(event_type, "open")      SHORTDESC "ET Open Count"           LONGDESC "Total opens on all emails"          KIND INT
    , count(event_type)        AS et_sendct             IF eq(event_type, "send")      SHORTDESC "ET Send Count"           LONGDESC "Total sends on all emails"          KIND INT
    , valuect(hourofday())     AS et_hourlyopen         IF eq(event_type, "open")      SHORTDESC "ET Hourly Opens"         LONGDESC "Which hours of the day the user is active"
    , valuect(hourofweek())    AS et_hourofweek         IF eq(event_type, "open")      SHORTDESC "ET Hour of Week Events"  LONGDESC "Which hours of the week the user is active"
    , valuect(yymm())          AS et_yymm               IF eq(event_type, "open")      SHORTDESC "ET Opens By Month" 
    , epochms()                AS et_lastclick_ts       IF eq(event_type, "click")     SHORTDESC "ET Last Click"           LONGDESC "Last time user clicked an email"        KIND DATE
    , min(epochms())           AS et_firstclick_ts      IF eq(event_type, "click")     SHORTDESC "ET First Click"          LONGDESC "First time user clicked an email"       KIND DATE
    , count(event_type)        AS et_clickct            IF eq(event_type, "click")     SHORTDESC "ET Click Count"          LONGDESC "Total clicks on all emails"             KIND INT
    , max(epochms())           AS et_unsub_ts           IF eq(event_type, "unsubscribe")     SHORTDESC "ET Unsub date"     LONGDESC "Unsubscribe Date"                       KIND DATE
    , valuect(send_id)         AS total_opens_by_message      IF eq(event_type, "open")      SHORTDESC "The number of opens for a message" LONGDESC "The number of times a user opened a specific email"
    , valuect(send_id)         AS total_clicks_by_message     IF eq(event_type, "click")     SHORTDESC "The number of clicks for a message" LONGDESC "The number of times a user clicked a specific email"

FROM
    exacttarget_events_history
INTO
    user
BY
    et_subscriber_keys
ALIAS
    user_exacttarget_events_history
' | jq '.'
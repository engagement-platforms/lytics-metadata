UNLOAD ('select datejoined as created_date,
              emailaddress as email,
              0 as list_id,
              'ExactTarget' as list_name,
              status,
              subscriberkey as subscriber_key
       from email.et_subscribers
       join email.stg_sublink using (subscriberid)
       union
       select createddate as created_date,
              emailaddress as email,
              listid as list_id,
              listname as list_name,
              ls.status,
              subscriberkey as subscriber_key
       from email.et_list_subscriber ls
       join email.stg_sublink using (subscriberid)')
TO 's3://atl-marketing/email-marketing/redshift-dump/dataset.et.subscribers.csv' 
CREDENTIALS 'aws_access_key_id=;aws_secret_access_key='
DELIMITER AS ',' 
ADDQUOTES 
PARALLEL OFF 
ALLOWOVERWRITE 
GZIP;
/*
Build a user from web data
 */
SELECT 
    join(instance,username,":")         AS user_id
    ,username							AS username  -- extract the user who fired the event
    ,instance                       	AS instance  -- extract the instance who the user was on when the event was fired


    -- JIRA Software
    ,set(attributes.rank)				AS jira_software_all_nps_scores
    											IF product == 'jira'
    											AND event == 'nps.survey.submit'


    ,attributes.rank					AS jira_software_latest_nps_score
    											IF product == 'jira'
    											AND event == 'nps.survey.submit'
    											KIND INT
    											MERGEOP latest

    ,set(attributes.role)				AS jira_software_all_nps_roles
    											IF product == 'jira'
    											AND event == 'nps.survey.submit'

	,attributes.role					AS jira_software_latest_nps_role
												IF product == 'jira'
    											AND event == 'nps.survey.submit'
    											MERGEOP latest

	,time_string						AS jira_software_last_nps_survey_completed
												IF product == 'jira'
												AND event == 'nps.survey.submit'
												KIND DATE
												MERGEOP latest

    -- Confluence				
	,set(attributes.rank)				AS confluence_all_nps_scores
    											IF product == 'confluence'
    											AND event == 'nps.survey.submit'

    ,attributes.rank					AS confluence_latest_nps_score
    											IF product == 'confluence'
    											AND event == 'nps.survey.submit'
    											KIND INT
    											MERGEOP latest

    ,set(attributes.role)				AS confluence_all_nps_roles
    											IF product == 'confluence'
    											AND event == 'nps.survey.submit'

	,attributes.role					AS confluence_latest_nps_role
												IF product == 'confluence'
    											AND event == 'nps.survey.submit'
    											MERGEOP latest

	,time_string						AS confluence_last_nps_survey_completed
												IF product == 'confluence'
												AND event == 'nps.survey.submit'
												KIND DATE
												MERGEOP latest

    -- JIRA Service Desk

    -- Bitbucket

    -- HipChat


--   QUESTIONS
-- 	How do we determine the difference between JSW, JSD and JCore?
-- 	Where do the HC + BB events live?



FROM
    in_product_events
INTO
    user
BY
    user_id -- from docs: Join together multiple values, coerce them into strings. Want to do this so we get unique users
ALIAS
    net_promoter_scores
/*
LQL for license data
*/

select
       -- we are going to use email to be an identity field
       email(tech_email) AS email,
       set(tech_email) AS tech_email,
       emaildomain(tech_email) AS email_domain,

       tech_city,
       tech_state,
       tech_country,

       -- most recent license info
       now()                            AS last_license_info        KIND DATE, 
       todate(paid_license_start_date)  AS paid_license_start_date  KIND DATE,
       todate(paid_license_end_date)    AS paid_license_end_date    KIND DATE,
       todate(eval_end_date)            AS eval_end_date            KIND DATE,
       todate(eval_start_date)          AS eval_start_date          KIND DATE,

       -- product info, complete list of product information
       -- keep a set of each
       set(product_family),
       set(platform),
       set(base_product),
       set(license_type),

       -- Date of expiration maps
       map(platform,eval_end_date)               AS platform_eval_end_date            KIND map[string]time,
       map(platform,eval_start_date)             AS platform_eval_start_date          KIND map[string]time,
       map(platform,paid_license_start_date)     AS platform_paid_license_start_date  KIND map[string]time,
       map(platform,paid_license_end_date)       AS platform_paid_license_end_date    KIND map[string]time,
       map(product,eval_end_date)                AS product_eval_end_date             KIND map[string]time,
       map(product,eval_start_date)              AS product_eval_start_date           KIND map[string]time,
       map(product,paid_license_start_date)      AS product_paid_license_start_date   KIND map[string]time,
       map(product,paid_license_end_date)        AS product_paid_license_end_date     KIND map[string]time,
       map(base_product,eval_end_date)           AS eval_end  IF eq(platform, "Cloud")   SHORTDESC "Cloud Evaluations End date"   KIND map[string]time,
       map(base_product,eval_start_date)         AS eval_start IF eq(platform, "Cloud")  SHORTDESC "Cloud Evaluations Start date" KIND map[string]time,
       map(base_product,paid_license_start_date) AS lic_start IF eq(platform, "Cloud")   SHORTDESC "Cloud License Start date"     KIND map[string]time,
       map(base_product,paid_license_end_date)   AS lic_end  IF eq(platform, "Cloud")   SHORTDESC "Cloud License End date"        KIND map[string]time,
       valuect(product)                          AS no_of_products_owned IF eq(platform, "Cloud") SHORTDESC "The number of products owned in cloud" ,
       valuect(base_product)                     AS no_of_base_products_owned IF eq(platform, "Cloud") SHORTDESC "The number of base products owned in cloud" ,
       valuect(product)                          AS no_of_products_owned IF eq(platform, "Server") SHORTDESC "The number of products owned in server" ,
       valuect(base_product)                     AS no_of_base_products_owned IF eq(platform, "Server") SHORTDESC "The number of base products owned in server" ,
       count(sen)                                AS cnt_jira_server IF eq(platform, "Server") AND eq(base_product, "JIRA") SHORTDESC "The number of jira server licenses",
       count(sen)                                AS cnt_conf_server IF eq(platform, "Server") AND eq(base_product, "Confluence") SHORTDESC "The number of confluence server licenses",
       count(sen)                                AS cnt_jira_cloud IF eq(platform, "Cloud") AND eq(base_product, "JIRA") SHORTDESC "The number of jira cloud licenses",
       count(sen)                                AS cnt_conf_cloud IF eq(platform,"Cloud") AND eq(base_product, "Confluence") SHORTDESC "The number of confluence cloud licenses",
       count(sen)                                AS tot_lic_owned_cl IF ne(sen,"") AND eq(platform, "Cloud") SHORTDESC "The number of total licenses owned in cloud",
       count(sen)                                AS tot_lic_owned_srvr IF ne(sen,"") AND eq(platform, "Server") SHORTDESC "The number of total licenses owned in server",
       valuect(license_type)                     AS no_of_license_types IF eq(platform, "Server") SHORTDESC "The number of  server license types",
       valuect(license_type)                     AS no_of_license_types IF eq(platform, "Cloud") SHORTDESC "The number of  cloud license types"

from socrates_product_license_tmp
into user
by email
ALIAS license_data
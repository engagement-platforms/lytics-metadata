/*

Anonymous to known user mapping - only works for evaluators (tech contacts)

*/
SELECT
    
    set(_uid)                                                         AS _uids                                                         SHORTDESC "Web Cookie Ids(all)"
    , _uid                                                                                                                               SHORTDESC "Web Cookie Id(current)"
    , email(email)                                                      AS email                                                         SHORTDESC "Email"
    , emaildomain(email)                                                AS emaildomain                                                   SHORTDESC "Email Domain"

FROM
    segment_anonymous_to_known
INTO
    user
BY
    email OR _uids
ALIAS
    user_segment_anonymous_to_known
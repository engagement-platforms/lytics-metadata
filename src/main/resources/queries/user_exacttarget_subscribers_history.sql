/*
ExactTarget Subscribers, this is for loading our historical data from Redshift to Lytics into the user profiles

*/
SELECT
    
    set(subscriber_key) AS et_subscriber_keys
    , set(list_id)      AS et_list_ids
    , set(list_name)    AS et_list_names
    , status            AS et_status
    , min(created_date)      AS et_created_date      SHORTDESC "ET created date" KIND DATE
    
    , email(email)      AS email
FROM
    exacttarget_subscribers_history
INTO
    user
BY
    email or et_subscriber_keys
ALIAS
    user_exacttarget_subscribers_history
/*
Sendgrid data
------------------

The sendgrid data comes from the Sendgrid events api

If the email used via that sendgrid api matches the `email` in user, it will be
combined into the user profile
*/
SELECT
    valuect(event)      AS sg_email_action                                 SHORTDESC  "Email Actions"          LONGDESC "Number of each type of action for this user from Sendgrid"
    , count(event)      AS sg_email_opens IF eq(tolower(event), "open")    SHORTDESC  "Email Opens"                   
    , valuect(category) AS sg_email_category                               SHORTDESC  "Email Categories"       
    , valuect(url)      AS sg_email_url                                    SHORTDESC  "Email Urls Clicked on"
    , valuect(hash(urlmain(url)))  AS hashedurls        IF eq(tolower(event), "open") OR eq(tolower(event), "click")  SHORTDESC "Hashed Urls Visited"  LONGDESC "Count of how many times a hashed url was visited"
         
       
    , epochms()              AS last_active_ts   IF eq(tolower(event), "open")     SHORTDESC "Last Active"                KIND DATE
    , epochms()              AS sg_last_open_ts  IF eq(tolower(event), "open")     SHORTDESC  "Email Last Open Date"      LONGDESC "The last date a user opened an email"       KIND DATE
    , valuect(hourofday())   AS sg_hourlyopens   IF eq(tolower(event), "open")     SHORTDESC  "Email Hourly Opens"        LONGDESC "Which hours of the day the user has opened email"
    
    , email(email)    AS email                                           SHORTDESC  "Email"   
    
FROM sendgrid
INTO user BY email
ALIAS user_sendgrid
# Build a user from web data
SELECT 
     instance                        AS instance_name  -- extract the instance who the user was on when the event was fired
    ,set(username)                   AS usernames  -- set of the users who are active on the instance
    ,count(username)                 AS number_of_users -- count how many users on this instance
    ,valuect(username)               AS activity_by_user -- count how many events each user has fired on this instance

    ---- GENERIC EVENTS - Across all products
    ,valuect(event)                  AS all_products_count_of_events_fired_all_time

    ,map(event, todate(receivedTime))   AS all_products_last_time_each_event_fired 
                                            KIND map[string]time

    ,receivedTime                      AS all_products_last_event_fired  -- store the last time that they fired an event
                                            KIND DATE

    ,receivedTime                      AS all_products_first_event_fired
                                            KIND DATE
                                            MERGEOP OLDEST

    ---- JIRA Software specific
    ,valuect(event)      AS jira_software_count_of_events_fired_all_time 
                                  IF product = "jira" 
                                  AND not(hasprefix(event,"servicedesk.")) -- ensure that we"re exlcuding service desk events
 
    ,receivedTime          AS jira_software_last_event_fired 
                                  IF product = "jira" 
                                  AND not(hasprefix(event,"servicedesk.")) -- see when the last time they used JIRA
                                  KIND DATE

    ,receivedTime          AS jira_software_first_event_fired
                                  IF  product == "jira"
                                  AND not(hasprefix(event,"servicedesk."))
                                  KIND DATE
                                  MERGEOP OLDEST

    ,receivedTime          AS jira_software_first_native_android_app_event_fired
                                  IF product == "jira"
                                  AND hasprefix(event, "jira.mobile.native.android")
                                  KIND DATE
                                  MERGEOP OLDEST

    ,receivedTime          AS jira_software_last_native_android_app_event_fired
                                  IF product == "jira"
                                  AND hasprefix(event, "jira.mobile.native.android")
                                  KIND DATE

    ,receivedTime          AS jira_software_first_native_ios_app_event_fired
                                  IF product == "jira"
                                  AND hasprefix(event, "jira.mobile.native.ios")
                                  KIND DATE
                                  MERGEOP OLDEST

    ,receivedTime          AS jira_software_last_native_ios_app_event_fired
                                  IF product == "jira"
                                  AND hasprefix(event, "jira.mobile.native.ios")
                                  KIND DATE

    ,receivedTime          AS jira_software_last_global_admin_event_fired -- the administration.navigate.tabs event has isAdmin: as a property. TRUE = global admin, FALSE = project admin
                                  IF product == "jira"
                                  AND event == "administration.navigate.tabs"
                                  AND attributes.isAdmin == TRUE
                                  KIND DATE

    ,receivedTime          AS jira_software_last_project_admin_only_event_fired -- the administration.navigate.tabs event has isAdmin: as a property. TRUE = global admin, FALSE = project admin
                                  IF product == "jira"
                                  AND event == "administration.navigate.tabs"
                                  AND attributes.isAdmin == FALSE
                                  KIND DATE

    ,count(event)         AS jira_software_issues_created 
                                  IF product == "jira"
                                  AND not(hasprefix(event,"servicedesk."))
                                  AND event = "issuecreated"


    ---- Confluence specific
    ,valuect(event)     AS confluence_count_of_events_fired_all_time 
                                  IF product = "confluence" -- store ALL events that have ever been fired, as well as a count

    ,receivedTime          AS confluence_last_event_fired 
                                  IF product = "confluence" -- see when the last time they used Confluence
                                  KIND DATE

    ,receivedTime          AS confluence_first_event_fired
                                  IF  product == "confluence"
                                  KIND DATE
                                  MERGEOP OLDEST

    ,receivedTime          AS confluence_first_native_android_app_event_fired
                                  IF product == "confluence"
                                  AND hasprefix(event, "confluence.mobile.native.android")
                                  KIND DATE
                                  MERGEOP OLDEST

    ,receivedTime          AS confluence_last_native_android_app_event_fired
                                  IF product == "confluence"
                                  AND hasprefix(event, "confluence.mobile.native.android")
                                  KIND DATE

    ,receivedTime          AS confluence_first_native_ios_app_event_fired
                                  IF product == "confluence"
                                  AND hasprefix(event, "confluence.mobile.native.ios")
                                  KIND DATE
                                  MERGEOP OLDEST

    ,receivedTime          AS confluence_last_native_ios_app_event_fired
                                  IF product == "confluence"
                                  AND hasprefix(event, "confluence.mobile.native.ios")
                                  KIND DATE

    ,count(event)         AS confluence_pages_created
                                  IF product == "confluence"
                                  AND event == "pagecreated" --- TODO: double check event name


    ---- JIRA Service Desk specific
    ,valuect(event)     AS jira_service_desk_count_of_events_fired_all_time 
                                  IF product = "jira" 
                                  AND hasprefix(event,"servicedesk.")-- store ALL events that have ever been fired, as well as a count

    ,receivedTime          AS jira_service_desk_last_event_fired 
                                  IF product = "jira" 
                                  AND hasprefix(event,"servicedesk.") -- see when the last time they used this product
                                  KIND DATE

    ,receivedTime          AS jira_service_desk_first_event_fired
                                  IF product == "jira"
                                  AND hasprefix(event,"servicedesk.")
                                  KIND DATE
                                  MERGEOP OLDEST

    ,receivedTime                 AS jira_service_desk_last_agent_event_fired
                                  IF product == "jira"
                                  AND hasprefix(event,"servicedesk.")
                                  AND contains(event, "agent")
                                  KIND BOOL

    ,receivedTime                 AS jira_service_desk_last_admin_event_fired
                                  IF product == "jira"
                                  AND hasprefix(event,"servicedesk.")
                                  AND contains(event, "admin")
                                  KIND BOOL

    ,timebucket(todate(receivedTime))   AS jira_service_desk_admin_events_by_days  -- create a timebucket of when the user last fired an admin event by EPOCH DAYS 
                                                IF product == "jira"
                                                AND hasprefix(event,"servicedesk.")
                                                AND contains(event,"admin")

    ,timebucket(todate(receivedTime))   AS jira_service_desk_agent_events_by_days -- create a timebucket of when the user last fired an admin event by EPOCH DAYS 
                                                IF product == "jira"
                                                AND hasprefix(event,"servicedesk.")
                                                AND contains(event,"agent")

    ---- Bitbucket specific
    ,valuect(event)     AS bitbucket_count_of_events_fired_all_time 
                                  IF product = "bitbucket" -- store ALL events that have ever been fired, as well as a count

    ,receivedTime          AS bitbucket_last_event_fired 
                                  IF product = "bitbucket" -- see when the last time they used this product
                                  KIND DATE

    ,receivedTime          AS bitbucket_first_event_fired
                                  IF  product == "bitbucket"
                                  KIND DATE
                                  MERGEOP OLDEST

    ---- HipChat specific
    ,valuect(event)      AS hipchat_count_of_events_fired_all_time 
                                  IF product = "hipchat" -- store ALL events that have ever been fired, as well as a count

    ,receivedTime          AS hipchat_last_event_fired 
                                  IF product = "hipchat" -- see when the last time they used this product
                                  KIND DATE

    ,receivedTime          AS hipchat_first_event_fired
                                  IF product == "hipchat"
                                  KIND DATE
                                  MERGEOP OLDEST


    ---- User Management / Crowd /  AID specific
    ,valuect(event)     AS crowd_count_of_events_fired_all_time 
                                  IF product = "crowd" -- store ALL events that have ever been fired, as well as a count

    ,valuect(event)     AS user_management_count_of_events_fired_all_time 
                                  IF product = "User Management" -- store ALL events that have ever been fired, as well as a count

    ,valuect(event)     AS atlassian_account_count_of_events_fired_all_time 
                                  IF product = "aid" -- store ALL events that have ever been fired, as well as a count

    ,receivedTime          AS identity_last_event_fired IF product IN ("User Management", "crowd", "aid") -- see when the last time they used this product
                                  KIND DATE  

    ,receivedTime          AS identity_first_event_fired
                                  IF  product IN ("User Management", "crowd", "aid")
                                  KIND DATE
                                  MERGEOP OLDEST
    
FROM
    in_product_events
INTO
    instance
BY
    instance_name
ALIAS
    instance_in_product_events
